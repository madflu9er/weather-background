const TEMP_1 = -10;
const TEMP_2 = 10;
const TEMP_3 = 30;

const COLOR_1 = [0,255,255];
const COLOR_2 = [255,247,0];
const COLOR_3 = [255,140,0];

export const SLIDER_WIDTH = 100;
export const GRADIENT = [
    [ TEMP_1, COLOR_1 ],
    [ TEMP_2, COLOR_2 ],
    [ TEMP_3, COLOR_3 ],
];