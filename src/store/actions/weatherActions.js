import { SET_WEATHER, SET_WEATHER_IMG } from "../types/weatherTypes";

export const setWeather = (w) => ({ type: SET_WEATHER, payload: w });

export const setWeatherImg = (img) => ({ type: SET_WEATHER_IMG, payload: img});
