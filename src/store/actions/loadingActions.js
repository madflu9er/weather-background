import { SHOW_LOADING, HIDE_LOADING, HIDE_DIALOG, SET_LOADING_TEXT } from "../types/loadingTypes";

export const hideLoading = () => ({ type: HIDE_LOADING });

export const showLoading = () => ({ type: SHOW_LOADING });

export const hideDialog = () => ({ type: HIDE_DIALOG });

export const setLoadingText = (text) => ({type: SET_LOADING_TEXT, payload: text});
