import { SET_GEOLOCATION } from "../types/geoLocationTypes";

export const setGeoLocation = (gl) => ({ type: SET_GEOLOCATION, payload: gl });
