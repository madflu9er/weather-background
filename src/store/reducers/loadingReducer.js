import {SHOW_LOADING, HIDE_LOADING, HIDE_DIALOG, SET_LOADING_TEXT} from "../types/loadingTypes";

const initialState = {
    isLoading: false,
    loadingText: "",
    isDialog: JSON.parse(localStorage.getItem("allowLocation")) ?? true,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SHOW_LOADING:
            return {...state, isLoading: true}
        case HIDE_LOADING:
            return {...state, isLoading: false, loadingText: ""}
        case HIDE_DIALOG:
            return {...state, isDialog: false}
        case SET_LOADING_TEXT:
            return {...state, loadingText: action.payload}
        default:
            return state
    }
};
