import { combineReducers } from 'redux';

import weather from './weatherReducer';
import loading from './loadingReducer';
import location from './geoLocation';

export default combineReducers({
    weather,
    loading,
    location
});
