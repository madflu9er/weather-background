import { SET_GEOLOCATION } from "../types/geoLocationTypes";

const initialState = {
    geo: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_GEOLOCATION:
            return {...state, geo: action.payload}
        default:
            return state
    }
};
