import { SET_WEATHER, SET_WEATHER_IMG } from "../types/weatherTypes";

const initialState = {
    weather: null,
    weatherImg: "",
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_WEATHER:
            return { ...state, weather: action.payload }
        case SET_WEATHER_IMG:
            return {...state, weatherImg: `https://www.metaweather.com/static/img/weather/png/${action.payload}.png`}
        default:
            return state
    }
};
