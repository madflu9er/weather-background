import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk'
import App from './App';
import rootReducer from "./store/reducers/root";
import { Provider } from "react-redux";
import { createStore, compose, applyMiddleware } from "redux";
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import './style/index.css';


const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, compose(
  applyMiddleware(thunk),
  composeEnhancers()
));

ReactDOM.render(
  <StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
