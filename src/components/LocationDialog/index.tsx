import React from "react";
import { connect } from 'react-redux'
import { getGL } from "../../utils/geolocation";
import '../../style/dialog.css';

const GeoDialog = ({allowGeoAccess}: any) => {
    return (
        <div className="dialog">
            <span>
                Allow to use geo location?
            </span>
            <div>
                <button onClick={allowGeoAccess}>Yes</button>
                <button>No</button>
            </div>
        </div>
    )
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
      allowGeoAccess: () => dispatch(getGL())
    }
}

export default connect(null, mapDispatchToProps)(GeoDialog);