import React from "react"
import "../../style/slider.css";

export default ({step, value, onChange, maxValue, minValue}: any) => {
    return (
        <div>
            <input
                className="slider"
                onChange={e => onChange(e.target.value)}
                type="range"
                min={minValue}
                max={maxValue}
                value={value}
                step={step}
            />
        </div>
    )
}