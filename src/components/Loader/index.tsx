import React from "react";
import { connect } from "react-redux";
import "../../style/loader.css";

const Loader = ({loadingText}: any) => (
    <div className="loader">
        <div className="loading spinner-border text-success" role="status"></div>
        <span>{loadingText}</span>
    </div>
)

const mapStateToProps = (state: any) => {
    return {
        loadingText: state.loading.loadingText
    }
}

export default connect(mapStateToProps)(Loader);