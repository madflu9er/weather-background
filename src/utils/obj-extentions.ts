export const cloneAsObject = (obj: any) => {
    if (obj === null || !(obj instanceof Object)) {
        return obj;
    }
    const temp: any = Array.isArray(obj) ? [] : {};

    for (const key in obj) {
        temp[key] = cloneAsObject(obj[key]);
    }
    return temp;
};
