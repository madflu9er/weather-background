import { GRADIENT, SLIDER_WIDTH } from "../store/constant";

const API = "https://www.metaweather.com/api";
const WITH_CORS = "https://cors-anywhere.herokuapp.com";

export const getCity = (latt: number, long: number) => {
    return fetch(`${WITH_CORS}/${API}/location/search/?lattlong=${latt},${long}`, {
        "method": "GET",
    })
    .then(response => {
        return response.json();
    })
    .catch(err => {
        console.log(err);
    });
}

export const getWeather = (cityId: number) => {
    return fetch(`${WITH_CORS}/${API}/location/${cityId}`, {
        "method": "GET"
    })
    .then(response => {
        return response.json();
    });
}

const pickHex = (color1: any, color2: any, weight: number) => {
    const p = weight;
    const w = p * 2 - 1;
    const w1 = (w/1+1) / 2;
    const w2 = 1 - w1;
    const rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
        Math.round(color1[1] * w1 + color2[1] * w2),
        Math.round(color1[2] * w1 + color2[2] * w2)];
    return rgb;
};

const getInterpolatedColor = (t: number, colorRange: Array<number>) => {
    const firstcolor = GRADIENT[colorRange[0]][1];
    const secondcolor = GRADIENT[colorRange[1]][1];

    const firstcolor_x = SLIDER_WIDTH * ((GRADIENT as any)[colorRange[0]][0]/100);
    const secondcolor_x = SLIDER_WIDTH * ((GRADIENT as any)[colorRange[1]][0]/100)-firstcolor_x;
    const slider_x = SLIDER_WIDTH * (t/100)-firstcolor_x;
    const ratio = slider_x/secondcolor_x;

    return pickHex(secondcolor,firstcolor, ratio );
};

export const getColorByTemperature = (t: number): string => {
    let colorRange: Array<number> = [];

    GRADIENT.forEach((gColor: any, index: number) => {
        if(Number(t) === -10)
            colorRange = [0, 1];
        if(Number(t) === 30)
            colorRange = [1, 2];
        if(Number(t) < gColor[0] && !colorRange.length)
            colorRange = [index-1, index];
    });

    const color = getInterpolatedColor(t, colorRange);

    return `rgb(${color})`;
}

