import { cloneAsObject } from "./obj-extentions";
import { hideDialog, showLoading, hideLoading, setLoadingText } from "../store/actions/loadingActions";
import { setGeoLocation } from "../store/actions/geoLocationActions";
import { getCity, getWeather } from "./weather";
import { setWeather, setWeatherImg } from "../store/actions/weatherActions";

const onGetLocation = (geoLocation: Position, dispatch: Function) => {
    localStorage.setItem("allowLocation", "false");

    dispatch(setGeoLocation(geoLocation));
    dispatch(setLoadingText("Getting city with geoposition..."))

    getCity(geoLocation.coords.latitude, geoLocation.coords.longitude)
    .then(cityData => {
        const city = cityData[0];
        dispatch(setLoadingText(`Getting temperature in ${city.title}`))
        getWeather(city.woeid).then(weatherData => {
            const weather = weatherData.consolidated_weather[0];

            dispatch(setWeather(weather));
            dispatch(setWeatherImg(weather.weather_state_abbr));
            dispatch(hideLoading());
        })
    });
};

const getChromeGeoLocation = (cb?: Function) => {
    navigator.geolocation.getCurrentPosition((geoData) => {
        const geoDataClone = cloneAsObject(geoData);
        if(cb)
            cb(geoDataClone);

        return geoDataClone;
    });
};

export const getGL = () => (dispatch: Function) => {
    dispatch(showLoading());
    dispatch(setLoadingText("Getting geoposition with Chrome..."));
    getChromeGeoLocation((gl: any) => onGetLocation(gl, dispatch));
    dispatch(hideDialog());
};
