import React, { useEffect, useState } from "react";
import AllowLocationDialog from "./components/LocationDialog";
import Loader from "./components/Loader";
import { connect } from "react-redux";
import "./style/App.css";
import { getGL } from "./utils/geolocation";
import Slider from "./components/Slider";
import { getColorByTemperature } from "./utils/weather";

function App({ isLoading, isDialog, getGeoData, weatherData, weatherImg }: any) {

	const [temperature, setTemperature] = useState(0);
	const [color, setColor] = useState("#fff");

	useEffect(() => {
		if(weatherData) {
			setColor(getColorByTemperature(weatherData.the_temp));
			setTemperature(weatherData.the_temp);
		}
	}, [weatherData]);

	useEffect(() => {
		if(!isDialog)
			getGeoData();
	}, [isDialog, getGeoData]);

	const changeColor = (val: number) => {
		setTemperature(val);
		setColor(getColorByTemperature(val))
	};

	if (isDialog)
		return <AllowLocationDialog />

	if(isLoading)
		return <Loader />

	return (
		<div style={{background: color}} className="App">
			<h1>temperature: {temperature}℃</h1>
			<Slider step={1} value={temperature} maxValue={30} minValue={-10} onChange={changeColor}/>
			<div className="weather-img">
				<img src={weatherImg} alt="Img"/>
			</div>
		</div>

	);
};

const mapStateToProps = (state: any) => {
	return {
		isLoading: state.loading.isLoading,
		isDialog: state.loading.isDialog,
		weatherData: state.weather.weather,
		weatherImg: state.weather.weatherImg
	}
};

const mapDispatchToProps = (dispatch: Function) => {
	return {
		getGeoData: () => dispatch(getGL())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
